# #1 Cardio Walking


![](images/2023-04-16-19-16-32.png)

30 seconds
![](images/2023-04-16-19-17-31.png)
![](images/2023-04-16-19-18-17.png)

single arm step back
![](images/2023-04-16-19-18-48.png)

big march
![](images/2023-04-16-19-19-52.png)

harmstring
![](images/2023-04-16-19-20-27.png)

tap left-right
![](images/2023-04-16-19-21-08.png)

march arms down-middle-up sideways
![](images/2023-04-16-19-21-50.png)

march rolling punch
![](images/2023-04-16-19-22-22.png)

squat hands around the world
![](images/2023-04-16-19-23-04.png)

scoties left-sideways hands both up
![](images/2023-04-16-19-23-59.png)